//import algorithm.tabu.ConflictingVerticesSearcher;
//import algorithm.tabu.TabuSearch;
//import model.Graph;
//import model.GraphVertex;
//import model.GraphVerticesColorClass;
//import model.TabuSearchGraphColoringSolution;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import reader.GraphDataReader;
//
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//public class TabuSearchTests {
//
//    private static final String TEST_CASE_FILE_NAME = "jean.col";
//    private static final Integer TEST_CASE_COLORS_COUNT = 10;
//    private static final Integer TEST_CASE_ITERATIONS = 10;
//    private static TabuSearchGraphColoringSolution solution;
//    private static Graph graph;
//
//    @BeforeAll
//    public static void init() {
//        TabuSearch tabuSearch = new TabuSearch();
//        GraphDataReader reader = new GraphDataReader();
//        graph = reader.load(TEST_CASE_FILE_NAME);
//        solution = tabuSearch.colorGraph(graph, TEST_CASE_COLORS_COUNT, TEST_CASE_ITERATIONS);
//    }
//
//    @Test
//    void testAllNodesArePresent() {
//        Set<Integer> expectedNodes = graph.getVertices().stream()
//                .map(GraphVertex::getNodeValue).collect(Collectors.toSet());
//        Set<Integer> actualNodes = solution.getColorClasses().stream()
//                .map(GraphVerticesColorClass::getVertices)
//                .flatMap(Set::stream)
//                .map(GraphVertex::getNodeValue)
//                .collect(Collectors.toSet());
//        assertEquals(expectedNodes, actualNodes);
//    }
//
//    @Test
//    void testColorClassesAreEquitable() {
//        List<GraphVerticesColorClass> colorClasses = solution.getColorClasses();
//        for (GraphVerticesColorClass firstColorClass : colorClasses) {
//            for (GraphVerticesColorClass secondColorCLass : colorClasses) {
//                if (firstColorClass == secondColorCLass) {
//                    continue;
//                }
//                assertTrue(Math.abs(firstColorClass.getVerticesCount() - secondColorCLass.getVerticesCount()) <= 1);
//            }
//        }
//    }
//
//    @Test
//    void testNodesInsideColorClassArentConflicting() {
//        List<GraphVertex> vertices = graph.getVertices();
//        for (GraphVertex vertex : vertices) {
//            int currentVertexColorClass = getVertexColorClass(vertex.getNodeValue());
//            List<Integer> neighbours = vertex.getNeighbours();
//            for (Integer neighbour : neighbours) {
//                assertNotEquals(currentVertexColorClass, getVertexColorClass(neighbour));
//            }
//        }
//    }
//
//    private int getVertexColorClass(int vertex) {
//        List<GraphVerticesColorClass> colorClasses = solution.getColorClasses();
//        for (int i = 0, colorClassesSize = colorClasses.size(); i < colorClassesSize; i++) {
//            GraphVerticesColorClass colorClass = colorClasses.get(i);
//            for (GraphVertex colorClassVertex : colorClass.getVertices()) {
//                if (colorClassVertex.getNodeValue() == vertex) {
//                    return i;
//                }
//            }
//        }
//        throw new RuntimeException("Node not found in color class");
//    }
//}
