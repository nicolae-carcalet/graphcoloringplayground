package model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.*;

@Data
public class TabuSearchGraphColoringSolution {
    private List<ColorClass> colorClasses;

    @EqualsAndHashCode.Exclude
    private Map<GraphVertex, AbstractMap.SimpleImmutableEntry<Integer, Integer>> graphMoves;

    public TabuSearchGraphColoringSolution(int numberOfColors) {
        colorClasses = new ArrayList<>();
        graphMoves = new HashMap<>();
        initColorClasses(numberOfColors);
    }

    private void initColorClasses(int numberOfColors) {
        for (int i = 0; i < numberOfColors; i++) {
            colorClasses.add(new ColorClass(i));
        }
    }

    public ColorClass getColorClassAt(int index) {
        return colorClasses.get(index);
    }

    public TabuSearchGraphColoringSolution copy() {
        int numberOfColorClasses = colorClasses.size();
        TabuSearchGraphColoringSolution newCopy = new TabuSearchGraphColoringSolution(numberOfColorClasses);
        newCopy.colorClasses = deepColorClassCopy();
        return newCopy;
    }

    private List<ColorClass> deepColorClassCopy() {
        List<ColorClass> copiedColorClasses = new ArrayList<>();
        for (ColorClass colorClass : colorClasses) {
            copiedColorClasses.add(colorClass.copy());
        }
        return copiedColorClasses;
    }

    public void addGraphMove(GraphVertex vertex, int oldColorClass, int newColorCLass) {
        graphMoves.put(vertex, new AbstractMap.SimpleImmutableEntry<>(oldColorClass, newColorCLass));
    }

    public void swapColor(int firstVertexColorClass, int secondVertexColorClass, GraphVertex firstVertex, GraphVertex secondVertex) {
        colorClasses.get(firstVertexColorClass).removeVertex(firstVertex);
        colorClasses.get(firstVertexColorClass).addVertex(secondVertex);
        colorClasses.get(secondVertexColorClass).removeVertex(secondVertex);
        colorClasses.get(secondVertexColorClass).addVertex(firstVertex);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Color classes: ").append(colorClasses.size()).append("\n");
        stringBuilder.append("Color classes values: ");
        for (ColorClass colorClass : colorClasses) {
            stringBuilder.append(colorClass.getVerticesCount()).append(" ");
        }
        stringBuilder.append("\n");
        for (ColorClass colorClass : colorClasses) {
            stringBuilder.append("Color class #").append(colorClass.getColorClassIndex()).append(": ");

            for (GraphVertex vertex : colorClass.getVertices()) {
                if (vertex.isInMaxClique()) {
                    stringBuilder.append(vertex.getNodeValue()).append("F, ");
                } else {
                    stringBuilder.append(vertex.getNodeValue()).append(", ");
                }
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
