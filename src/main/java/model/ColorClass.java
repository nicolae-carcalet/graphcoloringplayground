package model;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class ColorClass {

    private Set<GraphVertex> vertices;
    private int colorClassIndex;

    public ColorClass() {
        vertices = new HashSet<>();
    }

    public ColorClass(int index) {
        vertices = new HashSet<>();
        colorClassIndex = index;
    }

    public void addVertex(GraphVertex vertex) {
        vertices.add(vertex);
    }

    public int getVerticesCount() {
        return vertices.size();
    }

    public void removeVertex(GraphVertex graphVertex) {
        vertices.remove(graphVertex);
    }

    public ColorClass copy() {
        ColorClass graphVerticesColorClassCopy = new ColorClass();
        graphVerticesColorClassCopy.vertices = new HashSet<>(vertices);
        graphVerticesColorClassCopy.colorClassIndex = colorClassIndex;
        return graphVerticesColorClassCopy;
    }

    public int countNeighbours(Set<GraphVertex> graphVertexNeighbour) {
        int count = 0;
        for (GraphVertex vertex : vertices) {
            if (graphVertexNeighbour.contains(vertex)) {
                count++;
            }
        }
        return count;
    }
}
