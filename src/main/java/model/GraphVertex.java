package model;

import lombok.*;

@Data
@AllArgsConstructor
public class GraphVertex {
    private int nodeValue;

    @EqualsAndHashCode.Exclude
    private int coloringClass;

    @EqualsAndHashCode.Exclude
    private boolean isInMaxClique;
}
