package validation;

import lombok.extern.slf4j.Slf4j;
import model.ColorClass;
import model.GraphVertex;
import model.TabuSearchGraphColoringSolution;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultEdge;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class TabuSearchSolutionValidator {

    public void validateSolution(TabuSearchGraphColoringSolution tabuSearchGraphColoringSolution, Graph<GraphVertex, DefaultEdge> graph) {
        testAllNodesArePresent(tabuSearchGraphColoringSolution, graph);
        testColorClassesAreEquitable(tabuSearchGraphColoringSolution);
        testNodesInsideColorClassArentConflicting(graph);
    }

    void testAllNodesArePresent(TabuSearchGraphColoringSolution solution, Graph<GraphVertex, DefaultEdge> graph) {
        Set<Integer> expectedNodes = graph.vertexSet().stream()
                .map(GraphVertex::getNodeValue).collect(Collectors.toSet());
        Set<Integer> actualNodes = solution.getColorClasses().stream()
                .map(ColorClass::getVertices)
                .flatMap(Set::stream)
                .map(GraphVertex::getNodeValue)
                .collect(Collectors.toSet());
        if (expectedNodes.size() != actualNodes.size()) {
            throw new RuntimeException("Not all vertices are present");
        }
    }

    void testColorClassesAreEquitable(TabuSearchGraphColoringSolution solution) {
        List<ColorClass> colorClasses = solution.getColorClasses();
        for (ColorClass firstColorClass : colorClasses) {
            for (ColorClass secondColorCLass : colorClasses) {
                if (firstColorClass == secondColorCLass) {
                    continue;
                }
                if (Math.abs(firstColorClass.getVerticesCount() - secondColorCLass.getVerticesCount()) > 1) {
                    log.info("Color classes aren't equitable {} {}", firstColorClass.getVerticesCount(), secondColorCLass.getVerticesCount());
                    throw new RuntimeException("Color classes aren't equitable");
                }
            }
        }
    }

    void testNodesInsideColorClassArentConflicting(Graph<GraphVertex, DefaultEdge> graph) {
        Set<GraphVertex> vertices = graph.vertexSet();
        for (GraphVertex vertex : vertices) {
            for (GraphVertex neighbour : Graphs.neighborSetOf(graph, vertex)) {
                if (vertex.getColoringClass() == neighbour.getColoringClass()) {
                    log.info("Nodes are conflicting");
                    throw new RuntimeException("Adjacent nodes are in the same color class");
                }
            }
        }
    }
}
