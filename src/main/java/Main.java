import algorithm.tabu.TabuSearch;
import lombok.extern.slf4j.Slf4j;
import model.GraphVertex;
import model.TabuSearchGraphColoringSolution;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import reader.GraphDataReader;
import validation.TabuSearchSolutionValidator;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class Main {
    private static final TabuSearchSolutionValidator validator = new TabuSearchSolutionValidator();
    private static final TabuSearch tabuSearch = new TabuSearch();
    private static final GraphDataReader reader = new GraphDataReader();
    private static final int ITERATIONS = 1000;

    public static void main(String[] args) {

        Map<String, Integer> benchmarkData = new HashMap<>();
//        benchmarkData.put("DSJC125.5", 17);
//        benchmarkData.put("DSJC125.9", 44);
//        benchmarkData.put("DSJC250.1", 8);
//        benchmarkData.put("DSJC250.5", 30);
//        benchmarkData.put("DSJR500.1", 12);
//        benchmarkData.put("DSJR500.5", 119);
//        benchmarkData.put("R125.1", 5);
//        benchmarkData.put("R125.5", 36);
//        benchmarkData.put("R250.1", 8);
//        benchmarkData.put("R250.5", 65);
//        benchmarkData.put("queen5_5", 5);
//        benchmarkData.put("queen6_6", 7);
//        benchmarkData.put("queen7_7", 7);
//        benchmarkData.put("queen8_8", 9);
//        benchmarkData.put("queen9_9", 10);
//        benchmarkData.put("queen10_10", 11);
//        benchmarkData.put("queen12_12", 12);

//        benchmarkData.entrySet().forEach(Main::testInstance);

        Graph<GraphVertex, DefaultEdge> graph = reader.load("queen7_7.col");
        TabuSearchGraphColoringSolution solution = tabuSearch.colorGraph(graph, 7, 1000);
        validator.validateSolution(solution, graph);
        System.out.printf("Solution OK\n%s", solution);
    }

    private static void testInstance(Map.Entry<String, Integer> benchmarkEntry) {
        try {
            Graph<GraphVertex, DefaultEdge> graph = reader.load(benchmarkEntry.getKey() + ".col");
            TabuSearchGraphColoringSolution solution = tabuSearch.colorGraph(graph, benchmarkEntry.getValue(), ITERATIONS);
            validator.validateSolution(solution, graph);
            log.info("Test instance {} OK", benchmarkEntry);
        } catch (Exception exception) {
            log.info("Test instance {} NOT OK", benchmarkEntry);
        }
    }

}
