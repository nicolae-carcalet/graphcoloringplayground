package algorithm.initial;

import model.GraphVertex;
import model.TabuSearchGraphColoringSolution;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

public interface InitialSolutionGenerator {

    TabuSearchGraphColoringSolution generate(Graph<GraphVertex, DefaultEdge> graph, int numberOfColors);
}
