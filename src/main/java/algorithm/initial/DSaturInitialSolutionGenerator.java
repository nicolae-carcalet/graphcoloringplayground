package algorithm.initial;

import model.ColorClass;
import model.GraphVertex;
import model.TabuSearchGraphColoringSolution;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;

public class DSaturInitialSolutionGenerator extends GreedyInitialSolutionGenerator implements InitialSolutionGenerator {

    @Override
    public TabuSearchGraphColoringSolution generate(Graph<GraphVertex, DefaultEdge> graph, int numberOfColors) {
        TabuSearchGraphColoringSolution tabuSearchGraphColoringSolution = new TabuSearchGraphColoringSolution(numberOfColors);
        int maximumAllowedVerticesInColorClass = (int)Math.round(graph.vertexSet().size() / (double)numberOfColors);
        Set<GraphVertex> uncoloredVertices = new HashSet<>(graph.vertexSet());
        Map<GraphVertex, Integer> graphVertexIntegerLookup = buildSaturationLookup(graph);
        while (!uncoloredVertices.isEmpty()) {
            GraphVertex vertexWithLargestSaturationDegree = getVertexWithLargestSaturationDegree(uncoloredVertices, graphVertexIntegerLookup);
            uncoloredVertices.remove(vertexWithLargestSaturationDegree);
            addVertexInsideColorClass(graph, vertexWithLargestSaturationDegree, maximumAllowedVerticesInColorClass, tabuSearchGraphColoringSolution);
            updateSaturationLookup(graph, graphVertexIntegerLookup, vertexWithLargestSaturationDegree);
        }
        return tabuSearchGraphColoringSolution;
    }

    private void updateSaturationLookup(Graph<GraphVertex, DefaultEdge> graph, Map<GraphVertex, Integer> graphVertexIntegerLookup, GraphVertex vertex) {
        Set<GraphVertex> graphNeighbours = Graphs.neighborSetOf(graph, vertex);
        for (GraphVertex graphNeighbour : graphNeighbours) {
            graphVertexIntegerLookup.put(graphNeighbour, graphVertexIntegerLookup.get(graphNeighbour) + 1);
        }
    }

    private GraphVertex getVertexWithLargestSaturationDegree(Set<GraphVertex> uncoloredVertices, Map<GraphVertex, Integer> graphVertexIntegerLookup) {
        int maxSaturationDegree = -Integer.MAX_VALUE;
        GraphVertex graphVertex = null;
        for (GraphVertex uncoloredVertex : uncoloredVertices) {
            if (graphVertexIntegerLookup.get(uncoloredVertex) > maxSaturationDegree) {
                 maxSaturationDegree = graphVertexIntegerLookup.get(uncoloredVertex);
                 graphVertex = uncoloredVertex;
            }
        }
        return graphVertex;
    }

    private Map<GraphVertex, Integer> buildSaturationLookup(Graph<GraphVertex, DefaultEdge> graph) {
        Map<GraphVertex, Integer> lookup = new HashMap<>();
        for (GraphVertex graphVertex : graph.vertexSet()) {
            lookup.put(graphVertex, 0);
        }
        return lookup;
    }
}
