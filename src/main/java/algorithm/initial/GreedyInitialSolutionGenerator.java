package algorithm.initial;

import lombok.extern.slf4j.Slf4j;
import model.ColorClass;
import model.GraphVertex;
import model.TabuSearchGraphColoringSolution;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.alg.clique.BronKerboschCliqueFinder;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;
import java.util.List;

@Slf4j
public class GreedyInitialSolutionGenerator implements InitialSolutionGenerator {

    public TabuSearchGraphColoringSolution generate(Graph<GraphVertex, DefaultEdge> graph, int numberOfColors) {
        TabuSearchGraphColoringSolution tabuSearchGraphColoringSolution = new TabuSearchGraphColoringSolution(numberOfColors);
        Optional<Set<GraphVertex>> maxClique = getMaxClique(graph);
        fillColorClasses(graph, tabuSearchGraphColoringSolution, maxClique, numberOfColors);
        return tabuSearchGraphColoringSolution;
    }

    private void fillColorClasses(Graph<GraphVertex, DefaultEdge> graph, TabuSearchGraphColoringSolution tabuSearchGraphColoringSolution, Optional<Set<GraphVertex>> maxClique, int numberOfColors) {
        int maximumAllowedVerticesInColorClass = (int)Math.round(graph.vertexSet().size() / (double)numberOfColors);
        List<GraphVertex> unvisitedVertices = buildGraphVerticesLookup(graph);
        distributeMaxCliqueVertices(tabuSearchGraphColoringSolution, unvisitedVertices, maxClique);
        Collections.shuffle(unvisitedVertices);
        for (GraphVertex unvisitedVertex : unvisitedVertices) {
            addVertexInsideColorClass(graph, unvisitedVertex, maximumAllowedVerticesInColorClass, tabuSearchGraphColoringSolution);
        }
    }

    private void distributeMaxCliqueVertices(TabuSearchGraphColoringSolution tabuSearchGraphColoringSolution, List<GraphVertex> unvisitedVertices, Optional<Set<GraphVertex>> maxClique) {
        if (maxClique.isEmpty()) {
            return;
        }
        int currentColorClassIndex = 0;
        int colorClassCount = tabuSearchGraphColoringSolution.getColorClasses().size();
        for (GraphVertex graphVertex : maxClique.get()) {
            unvisitedVertices.remove(graphVertex);
            graphVertex.setInMaxClique(true);
            graphVertex.setColoringClass(currentColorClassIndex);
            tabuSearchGraphColoringSolution.getColorClassAt(currentColorClassIndex).addVertex(graphVertex);
            currentColorClassIndex = (currentColorClassIndex + 1) % colorClassCount;
        }
    }

    private Optional<Set<GraphVertex>> getMaxClique(Graph<GraphVertex, DefaultEdge> graph) {
        int maxCliqueSize = 0;
        Set<GraphVertex> maxClique = null;
        BronKerboschCliqueFinder<GraphVertex, DefaultEdge> cliqueFinder = new BronKerboschCliqueFinder<>(graph);
        for (Set<GraphVertex> clique : cliqueFinder) {
            if (clique.size() > maxCliqueSize) {
                maxCliqueSize = clique.size();
                maxClique = clique;
            }
        }
        return Optional.ofNullable(maxClique);
    }

    protected void addVertexInsideColorClass(Graph<GraphVertex, DefaultEdge> graph,
                                           GraphVertex vertex,
                                           int maximumAllowedVerticesInColorClass,
                                           TabuSearchGraphColoringSolution tabuSearchGraphColoringSolution) {
        ColorClass minNeighboursColorClass = getColorClassWithMinimumNeighbours(graph, vertex, maximumAllowedVerticesInColorClass, tabuSearchGraphColoringSolution);
        if (minNeighboursColorClass == null) {
            minNeighboursColorClass = getColorClassWithMinimumNeighbours(graph, vertex, maximumAllowedVerticesInColorClass + 1, tabuSearchGraphColoringSolution);
        }
        addVertexInColorClass(minNeighboursColorClass, vertex);
    }

    private ColorClass getColorClassWithMinimumNeighbours(Graph<GraphVertex, DefaultEdge> graph,
                                                          GraphVertex vertex,
                                                          int maximumAllowedVerticesInColorClass,
                                                          TabuSearchGraphColoringSolution tabuSearchGraphColoringSolution) {
        int minNeighboursColorClassCount = Integer.MAX_VALUE;
        ColorClass minNeighboursColorClass = null;
        Set<GraphVertex> graphVertexNeighbour = Graphs.neighborSetOf(graph, vertex);

        for (ColorClass colorClass : tabuSearchGraphColoringSolution.getColorClasses()) {
            if (colorClass.getVerticesCount() == maximumAllowedVerticesInColorClass) {
                continue;
            }
            int neighbourCount = colorClass.countNeighbours(graphVertexNeighbour);
            if (neighbourCount == 0) {
                return colorClass;
            } else {
                if (neighbourCount < minNeighboursColorClassCount) {
                    minNeighboursColorClassCount = neighbourCount;
                    minNeighboursColorClass = colorClass;
                }
            }
        }
        return minNeighboursColorClass;
    }


    private void addVertexInColorClass(ColorClass colorClass, GraphVertex graphVertex) {
        graphVertex.setColoringClass(colorClass.getColorClassIndex());
        colorClass.addVertex(graphVertex);
    }

    private List<GraphVertex> buildGraphVerticesLookup(Graph<GraphVertex, DefaultEdge> graph) {
        // just make a copy of it
        return new ArrayList<>(graph.vertexSet());
    }
}
