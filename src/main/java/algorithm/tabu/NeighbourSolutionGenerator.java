package algorithm.tabu;

import lombok.extern.slf4j.Slf4j;
import model.*;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;

@Slf4j
public class NeighbourSolutionGenerator {

    public List<TabuSearchGraphColoringSolution> generateNeighbourSolutions(Graph<GraphVertex, DefaultEdge> graph,
                                                                            Deque<TabuSearchGraphColoringSolution> tabuSearchMemory, TabuSearchGraphColoringSolution solution) {
        final List<TabuSearchGraphColoringSolution> neighbourSolutions = new ArrayList<>();
        generateNeighbourSolutionsWithOneExchange(graph.vertexSet(), tabuSearchMemory, neighbourSolutions, solution);
        generateNeighbourSolutionsWithTwoExchanges(neighbourSolutions, tabuSearchMemory, graph.vertexSet(), solution);
        return neighbourSolutions;
    }

    private void generateNeighbourSolutionsWithOneExchange(Set<GraphVertex> vertexSet,
                                                           Deque<TabuSearchGraphColoringSolution> tabuSearchMemory,
                                                           List<TabuSearchGraphColoringSolution> neighbourSolutions,
                                                           TabuSearchGraphColoringSolution solution) {
        for (GraphVertex graphVertex : vertexSet) {
            if (graphVertex.isInMaxClique()) {
                continue;
            }
            ColorClass vertexColorClass = solution.getColorClassAt(graphVertex.getColoringClass());
            for (ColorClass colorClass : solution.getColorClasses()) {
                if (colorClass.getVerticesCount() < vertexColorClass.getVerticesCount()) {
                    TabuSearchGraphColoringSolution oneWayExchangeSolution = generateOneWayExchangeSolution(graphVertex, graphVertex.getColoringClass(), colorClass.getColorClassIndex(), solution);
                    if (!tabuSearchMemory.contains(oneWayExchangeSolution)) {
                        neighbourSolutions.add(oneWayExchangeSolution);
                    }
                }
            }
        }
    }

    private TabuSearchGraphColoringSolution generateOneWayExchangeSolution(GraphVertex vertex,
                                                                           int vertexColorClassIndex,
                                                                           int currentColorClassIndex,
                                                                           TabuSearchGraphColoringSolution solution) {
        TabuSearchGraphColoringSolution solutionCopy = solution.copy();
        solutionCopy.getColorClassAt(vertexColorClassIndex).removeVertex(vertex);
        solutionCopy.getColorClassAt(currentColorClassIndex).addVertex(vertex);
        solutionCopy.addGraphMove(vertex, vertexColorClassIndex, currentColorClassIndex);
        return solutionCopy;
    }

    private void generateNeighbourSolutionsWithTwoExchanges(List<TabuSearchGraphColoringSolution> neighbourSolutions,
                                                            Deque<TabuSearchGraphColoringSolution> tabuSearchMemory,
                                                            Set<GraphVertex> vertexSet,
                                                            TabuSearchGraphColoringSolution solution) {
        for (GraphVertex firstVertex : vertexSet) {
            for (GraphVertex secondVertex : vertexSet) {
                if ((firstVertex.isInMaxClique() && secondVertex.isInMaxClique())
                        || firstVertex == secondVertex
                        || firstVertex.getColoringClass() == secondVertex.getColoringClass()) {
                    continue;
                }
                TabuSearchGraphColoringSolution solutionCopy = solution.copy();
                solutionCopy.swapColor(firstVertex.getColoringClass(),
                        secondVertex.getColoringClass(),
                        firstVertex,
                        secondVertex);
                if(!tabuSearchMemory.contains(solutionCopy)) {
                    solutionCopy.addGraphMove(firstVertex, firstVertex.getColoringClass(), secondVertex.getColoringClass());
                    solutionCopy.addGraphMove(secondVertex, secondVertex.getColoringClass(), firstVertex.getColoringClass());
                    neighbourSolutions.add(solutionCopy);
                }
            }
        }
    }
}
