package algorithm.tabu;

import algorithm.initial.DSaturInitialSolutionGenerator;
import algorithm.initial.GreedyInitialSolutionGenerator;
import algorithm.initial.InitialSolutionGenerator;
import lombok.extern.slf4j.Slf4j;
import model.GraphVertex;
import model.TabuSearchGraphColoringSolution;
import constants.TabuConstants;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;

@Slf4j
public class TabuSearch {

    private final InitialSolutionGenerator initialSolutionGenerator;
    private final NeighbourSolutionGenerator neighbourSolutionGenerator;
    private final ConflictingVerticesSearcher conflictingVerticesSearcher;

    public TabuSearch() {
        initialSolutionGenerator = new DSaturInitialSolutionGenerator();
        conflictingVerticesSearcher = new ConflictingVerticesSearcher();
        neighbourSolutionGenerator = new NeighbourSolutionGenerator();
    }

    public TabuSearchGraphColoringSolution colorGraph(Graph<GraphVertex, DefaultEdge> graph, int numberOfColors, int numberOfIterations) {
        Deque<TabuSearchGraphColoringSolution> tabuSearchMemory = new ArrayDeque<>();
        TabuSearchGraphColoringSolution solution = initialSolutionGenerator.generate(graph, numberOfColors);
        for (int currentIteration = 0; currentIteration < numberOfIterations; currentIteration++) {
            updateTabuSearch(tabuSearchMemory);
            applyGraphMoves(solution);
            Set<GraphVertex> conflictingVertices = conflictingVerticesSearcher.getConflictingVertices(graph, solution);
            if (conflictingVertices.isEmpty()) {
                break;
            }
            List<TabuSearchGraphColoringSolution> neighbourSolutions = neighbourSolutionGenerator.generateNeighbourSolutions(graph, tabuSearchMemory, solution);
            log.info("Current iteration {} with {} conflicting vertices and {} neighbourhood solutions", currentIteration, conflictingVertices.size(), neighbourSolutions.size());
            TabuSearchGraphColoringSolution betterNeighbourSolution = getBestNeighbourSolution(graph, neighbourSolutions);
            if (betterNeighbourSolution == null) {
//                solution = initialSolutionGenerator.generate(graph, numberOfColors);
//                continue;
                break;
            }
            solution = betterNeighbourSolution;
            tabuSearchMemory.addLast(solution);
        }
        return solution;
    }

    private void updateTabuSearch(Deque<TabuSearchGraphColoringSolution> tabuSearchMemory) {
        if (tabuSearchMemory.size() >= TabuConstants.MAX_TABU_SEARCH_MEMORY_SIZE) {
            tabuSearchMemory.removeFirst();
        }
    }

    private TabuSearchGraphColoringSolution getBestNeighbourSolution(Graph<GraphVertex, DefaultEdge> graph,
                                                                     List<TabuSearchGraphColoringSolution> neighbourSolutions) {
        if (neighbourSolutions.isEmpty()) {
            return null;
        }
        int bestConflictingVerticesCountSoFar = Integer.MAX_VALUE;
        TabuSearchGraphColoringSolution bestNeighbourSolutionSoFar = null;
        for (TabuSearchGraphColoringSolution neighbourSolution : neighbourSolutions) {
            applyGraphMoves(neighbourSolution);
            Set<GraphVertex> conflictingVertices = conflictingVerticesSearcher.getConflictingVertices(graph, neighbourSolution);
            int currentConflictingVerticesSize = conflictingVertices.size();
            if (currentConflictingVerticesSize < bestConflictingVerticesCountSoFar) {
                bestConflictingVerticesCountSoFar = currentConflictingVerticesSize;
                bestNeighbourSolutionSoFar = neighbourSolution;
            }
            revertGraphMoves(neighbourSolution);
        }
        return bestNeighbourSolutionSoFar;
    }

    private void applyGraphMoves(TabuSearchGraphColoringSolution solution) {
        Map<GraphVertex, AbstractMap.SimpleImmutableEntry<Integer, Integer>> graphMoves = solution.getGraphMoves();
        if (graphMoves.isEmpty()) {
            return;
        }
        for (Map.Entry<GraphVertex, AbstractMap.SimpleImmutableEntry<Integer, Integer>> graphMove : graphMoves.entrySet()) {
            GraphVertex vertex = graphMove.getKey();
            AbstractMap.SimpleImmutableEntry<Integer, Integer> colorClasses = graphMove.getValue();
            int newColorClass = colorClasses.getValue();
            vertex.setColoringClass(newColorClass);
        }
    }

    private void revertGraphMoves(TabuSearchGraphColoringSolution solution) {
        Map<GraphVertex, AbstractMap.SimpleImmutableEntry<Integer, Integer>> graphMoves = solution.getGraphMoves();
        if (graphMoves.isEmpty()) {
            return;
        }
        for (Map.Entry<GraphVertex, AbstractMap.SimpleImmutableEntry<Integer, Integer>> graphMove : graphMoves.entrySet()) {
            GraphVertex vertex = graphMove.getKey();
            AbstractMap.SimpleImmutableEntry<Integer, Integer> colorClasses = graphMove.getValue();
            int oldColorCLass = colorClasses.getKey();
            vertex.setColoringClass(oldColorCLass);
        }
    }
}
