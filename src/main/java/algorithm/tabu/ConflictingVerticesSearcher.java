package algorithm.tabu;

import model.*;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.DefaultEdge;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConflictingVerticesSearcher {

    public Set<GraphVertex> getConflictingVertices(Graph<GraphVertex, DefaultEdge> graph, TabuSearchGraphColoringSolution solution) {
        Set<GraphVertex> conflictingVertices = new HashSet<>();
        List<ColorClass> colorClasses = solution.getColorClasses();
        for (ColorClass colorClass : colorClasses) {
            for (GraphVertex vertex : colorClass.getVertices()) {
                boolean isCurrentVertexConflicting = false;
                for (GraphVertex neighbourVertex : Graphs.neighborSetOf(graph, vertex)) {
                    if (vertex.getColoringClass() == neighbourVertex.getColoringClass()) {
                        isCurrentVertexConflicting = true;
                        conflictingVertices.add(neighbourVertex);
                    }
                }
                if (isCurrentVertexConflicting) {
                    conflictingVertices.add(vertex);
                }
            }
        }
        return conflictingVertices;
    }
}
