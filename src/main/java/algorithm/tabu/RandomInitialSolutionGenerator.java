package algorithm.tabu;

import model.ColorClass;
import model.GraphVertex;
import model.TabuSearchGraphColoringSolution;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;

public class RandomInitialSolutionGenerator {

    public TabuSearchGraphColoringSolution generate(Graph<GraphVertex, DefaultEdge> graph, int numberOfColors) {
        Random random = new Random(System.currentTimeMillis());
        TabuSearchGraphColoringSolution tabuSearchGraphColoringSolution = new TabuSearchGraphColoringSolution(numberOfColors);
        int maximumAllowedVerticesInColorClass = (int)Math.round(graph.vertexSet().size() / (double)numberOfColors);
        List<GraphVertex> graphVertices = buildGraphVerticesLookup(graph);
        Collections.shuffle(graphVertices);
        int currentVertex = 0;
        while (currentVertex < graphVertices.size()) {
            boolean foundColorClass = false;
            GraphVertex graphVertex = graphVertices.get(currentVertex++);
            for (ColorClass colorClass : tabuSearchGraphColoringSolution.getColorClasses()) {
                if (colorClass.getVerticesCount() < maximumAllowedVerticesInColorClass) {
                    colorClass.addVertex(graphVertex);
                    foundColorClass = true;
                    break;
                }
            }

            if(!foundColorClass) {
                foundColorClass = false;
                while (!foundColorClass) {
                    ColorClass randomColorClass = tabuSearchGraphColoringSolution.getColorClassAt(random.nextInt(tabuSearchGraphColoringSolution.getColorClasses().size()));
                    if (Math.abs(randomColorClass.getVerticesCount() - maximumAllowedVerticesInColorClass) < 1) {
                        foundColorClass = true;
                        graphVertex.setColoringClass(randomColorClass.getColorClassIndex());
                        randomColorClass.addVertex(graphVertex);
                    }
                }

            }
        }
        return tabuSearchGraphColoringSolution;
    }

    private List<GraphVertex> buildGraphVerticesLookup(Graph<GraphVertex, DefaultEdge> graph) {
        List<GraphVertex> vertices = new ArrayList<>();

        for (GraphVertex vertex : graph.vertexSet()) {
            vertices.add(vertex);
        }
        return vertices;
    }
}
