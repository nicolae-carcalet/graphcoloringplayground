package reader;

import lombok.extern.slf4j.Slf4j;
import model.GraphVertex;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
public class GraphDataReader {

    private static final String GRAPH_RESOURCE_FILE_SUFFIX = ".col";
    private static final String EMPTY_LINE = "";
    private static final char VERTEX_LINE_PREFIX = 'p';
    private static final char EDGE_LINE_PREFIX = 'e';
    private static final String GRAPH_DATA_FOLDER = "/data";

    public Graph<GraphVertex, DefaultEdge> load(String fileName) {
        if (!fileName.endsWith(GRAPH_RESOURCE_FILE_SUFFIX)) {
            throw new IllegalArgumentException("The name for the resouces should end with .col");
        }
        return readGraph(fileName);
    }

    private Graph<GraphVertex, DefaultEdge> readGraph(String fileName) {
        try {
            Graph<GraphVertex, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);
            List<String> inputFileContent = getInputFileLines(fileName);
            for (String line : inputFileContent) {
                if (isEmptyLine(line)) {
                    continue;
                }
                parseLine(line, graph);
            }
            return graph;
        } catch (Exception exception) {
            log.error("Exception thrown while reading graph from file {}", fileName, exception);
            throw new RuntimeException("Couldn't build graph", exception);
        }
    }

    private List<String> getInputFileLines(String fileName) throws URISyntaxException, IOException {
        URI fileURI = getClass().getResource(GRAPH_DATA_FOLDER + File.separator + fileName).toURI();
        Path path = Paths.get(fileURI);
        return Files.readAllLines(path);
    }

    private boolean isEmptyLine(String line) {
        return line.trim().equals(EMPTY_LINE);
    }

    private void addVertices(Graph<GraphVertex, DefaultEdge> graph, int count) {
        for (int vertexIndex = 0; vertexIndex < count; vertexIndex++) {
            graph.addVertex(new GraphVertex(vertexIndex, 0, false));
        }
    }

    private String[] getLineTokens(String line) {
        return line.split("\\s+");
    }

    private void parseLine(String line, Graph<GraphVertex, DefaultEdge> graph) {
        char start = line.charAt(0);
        String[] tokens = getLineTokens(line);
        if (start == VERTEX_LINE_PREFIX) {
            int vertexCount = Integer.parseInt(tokens[2]);
            addVertices(graph, vertexCount);
        } else if (start == EDGE_LINE_PREFIX) {
            parseEdgeLine(graph, tokens);
        }
    }

    private void parseEdgeLine(Graph<GraphVertex, DefaultEdge> graph, String[] lineTokens) {
        int firstVertexIndex = Integer.parseInt(lineTokens[1]) - 1;
        int secondVertexIndex = Integer.parseInt(lineTokens[2]) - 1;
        if (firstVertexIndex != secondVertexIndex) {
            GraphVertex firstGraphVertex = getGraphVertex(graph, firstVertexIndex);
            GraphVertex secondGraphVertex = getGraphVertex(graph, secondVertexIndex);
            graph.addEdge(firstGraphVertex, secondGraphVertex);
        }
    }

    private GraphVertex getGraphVertex(Graph<GraphVertex, DefaultEdge> graph, final int graphVertexValue) {
        return graph.vertexSet()
                .stream()
                .filter(graphVertex -> graphVertex.getNodeValue() == graphVertexValue)
                .findFirst()
                .orElse(null);
    }
}
